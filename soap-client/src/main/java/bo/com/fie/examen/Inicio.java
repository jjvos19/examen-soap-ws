package bo.com.fie.examen;

import java.util.List;

import bo.com.fie.examen.clientes.ClienteServicesService;
import bo.com.fie.examen.clientes.IClienteService;
import bo.com.fie.examen.clientes.Persona;

public class Inicio {

	public static void main(String[] args) {
		ClienteServicesService service = new ClienteServicesService();
		IClienteService cliente = service.getClienteServicesPort();
		int numero = 0;
		boolean bandera;
		for (int i = 0; i < 10; i++) {
			numero = i + 1;
			bandera = cliente.adicionar("Juan_" + numero, "Val_" + numero, "Or_" + numero);
			System.out.println((bandera ? "Se" : "NO se") + " adiciona");
		}

		List<Persona> lista = cliente.listarTodos();
		for (Persona p : lista) {
			System.out.println(String.format("%s %s %s", p.getNombre(), p.getPaterno(), p.getMaterno()));
		}
	}
}
