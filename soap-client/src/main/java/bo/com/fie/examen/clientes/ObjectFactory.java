
package bo.com.fie.examen.clientes;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.fie.examen.clientes package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Limpiar_QNAME = new QName("http://services.examen.fie.com.bo/", "limpiar");
    private final static QName _ListarTodosResponse_QNAME = new QName("http://services.examen.fie.com.bo/", "listarTodosResponse");
    private final static QName _LimpiarResponse_QNAME = new QName("http://services.examen.fie.com.bo/", "limpiarResponse");
    private final static QName _AdicionarResponse_QNAME = new QName("http://services.examen.fie.com.bo/", "adicionarResponse");
    private final static QName _Adicionar_QNAME = new QName("http://services.examen.fie.com.bo/", "adicionar");
    private final static QName _ListarTodos_QNAME = new QName("http://services.examen.fie.com.bo/", "listarTodos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.fie.examen.clientes
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Adicionar }
     * 
     */
    public Adicionar createAdicionar() {
        return new Adicionar();
    }

    /**
     * Create an instance of {@link ListarTodos }
     * 
     */
    public ListarTodos createListarTodos() {
        return new ListarTodos();
    }

    /**
     * Create an instance of {@link Limpiar }
     * 
     */
    public Limpiar createLimpiar() {
        return new Limpiar();
    }

    /**
     * Create an instance of {@link ListarTodosResponse }
     * 
     */
    public ListarTodosResponse createListarTodosResponse() {
        return new ListarTodosResponse();
    }

    /**
     * Create an instance of {@link LimpiarResponse }
     * 
     */
    public LimpiarResponse createLimpiarResponse() {
        return new LimpiarResponse();
    }

    /**
     * Create an instance of {@link AdicionarResponse }
     * 
     */
    public AdicionarResponse createAdicionarResponse() {
        return new AdicionarResponse();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Limpiar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "limpiar")
    public JAXBElement<Limpiar> createLimpiar(Limpiar value) {
        return new JAXBElement<Limpiar>(_Limpiar_QNAME, Limpiar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTodosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "listarTodosResponse")
    public JAXBElement<ListarTodosResponse> createListarTodosResponse(ListarTodosResponse value) {
        return new JAXBElement<ListarTodosResponse>(_ListarTodosResponse_QNAME, ListarTodosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LimpiarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "limpiarResponse")
    public JAXBElement<LimpiarResponse> createLimpiarResponse(LimpiarResponse value) {
        return new JAXBElement<LimpiarResponse>(_LimpiarResponse_QNAME, LimpiarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "adicionarResponse")
    public JAXBElement<AdicionarResponse> createAdicionarResponse(AdicionarResponse value) {
        return new JAXBElement<AdicionarResponse>(_AdicionarResponse_QNAME, AdicionarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Adicionar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "adicionar")
    public JAXBElement<Adicionar> createAdicionar(Adicionar value) {
        return new JAXBElement<Adicionar>(_Adicionar_QNAME, Adicionar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTodos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.examen.fie.com.bo/", name = "listarTodos")
    public JAXBElement<ListarTodos> createListarTodos(ListarTodos value) {
        return new JAXBElement<ListarTodos>(_ListarTodos_QNAME, ListarTodos.class, null, value);
    }

}
