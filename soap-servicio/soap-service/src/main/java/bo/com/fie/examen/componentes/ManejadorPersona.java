package bo.com.fie.examen.componentes;

import bo.com.fie.examen.entidades.Persona;

public class ManejadorPersona {

	public static Persona crearPersona(String nombre, String paterno, String materno) {
		Persona p = new Persona();
		p.setMaterno(materno.toUpperCase());
		p.setNombre(nombre.toUpperCase());
		p.setPaterno(paterno.toUpperCase());
		return p;
	}
}
