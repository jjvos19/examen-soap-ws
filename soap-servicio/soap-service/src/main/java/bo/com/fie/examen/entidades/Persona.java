package bo.com.fie.examen.entidades;

import java.io.Serializable;

public class Persona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String paterno;
	private String materno;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}
	
	public String claveHash() {
		String cuerpo = "";
		if (nombre != null) {
			cuerpo +=nombre.hashCode();
		}
		if (paterno != null) {
			cuerpo += paterno.hashCode();
		}
		if (materno != null) {
			cuerpo += materno.hashCode();
		}
		
		return cuerpo;
	}

}
