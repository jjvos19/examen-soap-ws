package bo.com.fie.examen.services;

import java.util.LinkedList;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import bo.com.fie.examen.componentes.ManejadorPersona;
import bo.com.fie.examen.entidades.Persona;

@WebService(endpointInterface = "bo.com.fie.examen.services.IClienteService")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class ClienteServices implements IClienteService {

	private static List<Persona> lista = null;
	private static List<String> existe = null;

	public ClienteServices() {
		if (lista == null) {
			lista = new LinkedList();
			existe = new LinkedList();
		}
	}

	public boolean adicionar(String nombre, String paterno, String materno) {
		// TODO Auto-generated method stub
		Persona p = ManejadorPersona.crearPersona(nombre, paterno, materno);
		String clave = p.claveHash();
		boolean banderaExiste = existe.contains(clave);
		System.out.println("Existe: " + banderaExiste + " - " + clave);
		if (!banderaExiste) {
			lista.add(p);
			existe.add(clave);
		}

		return !banderaExiste;
	}

	public List<Persona> listarTodos() {
		// TODO Auto-generated method stub
		return lista;
	}

	public boolean limpiar() {
		lista.clear();
		existe.clear();
		return true;
	}

}
