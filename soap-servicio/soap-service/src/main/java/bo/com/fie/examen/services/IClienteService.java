package bo.com.fie.examen.services;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import bo.com.fie.examen.entidades.Persona;

@WebService
public interface IClienteService {
	boolean adicionar(@WebParam(name = "nombre") String nombre, @WebParam(name = "apellidoPaterno") String paterno,
			@WebParam(name = "apellidoMaterno") String materno);

	@WebResult(name = "listaPersonas")
	List<Persona> listarTodos();

	boolean limpiar();
}
